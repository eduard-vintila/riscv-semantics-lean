import Mathlib
import Init.Prelude
import RiscvLean.LoVelib

open LoVe


/- # Helpers -/
def twosComplement (x : UInt32) := ~~~x + 1
instance : Neg UInt32 where
  neg := twosComplement

-- Coercions
instance : Coe UInt8 UInt16 where
  coe x := x.toUInt16
instance : Coe UInt8 UInt32 where
  coe x := x.toUInt32
instance : Coe UInt16 UInt32 where
  coe x := x.toUInt32

instance : Coe UInt32 UInt8 where
  coe x := x.toUInt8
instance : Coe UInt32 UInt16 where
  coe x := x.toUInt16
instance : Coe UInt16 UInt8 where
  coe x := x.toUInt8

instance : Coe Nat UInt8 where
  coe x := x.toUInt8
instance : Coe Nat UInt16 where
  coe x := x.toUInt16
instance : Coe Nat UInt32 where
  coe x := x.toUInt32

def combineBytes2 (x : UInt8) (y : UInt8) : UInt16 :=
  let b1 := x.toUInt16
  let b2 := y.toUInt16
  ((b1 <<< 8) ||| b2)

--#eval combineBytes2 0b10000000 0b00000001

def combineBytes4 (m : UInt8) (n : UInt8) (p : UInt8) (q : UInt8) : UInt32 :=
  let b1 := m.toUInt32
  let b2 := n.toUInt32
  let b3 := p.toUInt32
  let b4 := q.toUInt32
  ((b1 <<< 24) |||  (b2 <<< 16) ||| (b3 <<< 8) ||| b4)

def signExtend8 (x : UInt8) : UInt32 :=
    let msb := x &&& (1 <<< 7).toUInt8
    if msb ≠ 0 then (0xFFFFFF00 ||| x.toUInt32) else x.toUInt32

def signExtend16 (x : UInt16) : UInt32 :=
    let msb := x &&& (1 <<< 15).toUInt16
    if (msb ≠ 0) then (0xFFFF0000 ||| x.toUInt32) else x.toUInt32

/- # Data types -/
abbrev MachineInt : Type := UInt32
abbrev MachineAddr : Type := UInt32
abbrev Imm : Type := UInt32
abbrev Label : Type := String

/-
  # Register definitions
 * registers are defined according to their symbolic name
-/
inductive Register : Type where
  | x0 -- always holds 0
  | ra
  | sp
  | gp
  | tp
  | t0
  | t1
  | t2
  | fp
  | s1
  | a0
  | a1
  | a2
  | a3
  | a4
  | a5
  | a6
  | a7
  | s2
  | s3
  | s4
  | s5
  | s6
  | s7
  | s8
  | s9
  | s10
  | s11
  | t3
  | t4
  | t5
  | t6
deriving DecidableEq

/-
  # Language statements
  These include:
  * user-level `RV32I` instructions (except Auipc, Jal and Jalr - for these instructions, implementation of PC is necessary)
  * a `skip` statement, used as a nop or to mark a final instruction in a program
  * a `seq` statement (noted as a right associative ';' operator) for sequencing instructions in a program
-/
inductive Stmt : Type where
 | Lb : (rd : Register) -> (rs1 : Register) -> (oimm12 : Imm) -> Stmt
 | Lh : (rd : Register) -> (rs1 : Register) -> (oimm12 : Imm) -> Stmt
 | Lw : (rd : Register) -> (rs1 : Register) -> (oimm12 : Imm) -> Stmt
 | Lbu : (rd : Register) -> (rs1 : Register) -> (oimm12 : Imm) -> Stmt
 | Lhu : (rd : Register) -> (rs1 : Register) -> (oimm12 : Imm) -> Stmt
 | Addi : (rd : Register) -> (rs1 : Register) -> (imm12 : Imm) -> Stmt
 | Slli : (rd : Register) -> (rs1 : Register) -> (shamt6 : Imm) -> Stmt
 | Slti : (rd : Register) -> (rs1 : Register) -> (imm12 : Imm) -> Stmt
 | Sltiu : (rd : Register) -> (rs1 : Register) -> (imm12 : Imm) -> Stmt
 | Xori : (rd : Register) -> (rs1 : Register) -> (imm12 : Imm) -> Stmt
 | Ori : (rd : Register) -> (rs1 : Register) -> (imm12 : Imm) -> Stmt
 | Andi : (rd : Register) -> (rs1 : Register) -> (imm12 : Imm) -> Stmt
 | Srli : (rd : Register) -> (rs1 : Register) -> (shamt6 : Imm) -> Stmt
 | Srai : (rd : Register) -> (rs1 : Register) -> (shamt6 : Imm) -> Stmt

-- | Auipc : (rd : Register) -> (oimm20 : Imm) -> Stmt

 | Sb : (rs1 : Register) -> (rs2 : Register) -> (simm12 : Imm) -> Stmt
 | Sh : (rs1 : Register) -> (rs2 : Register) -> (simm12 : Imm) -> Stmt
 | Sw : (rs1 : Register) -> (rs2 : Register) -> (simm12 : Imm) -> Stmt

 | Add : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | Sub : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | Sll : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | Slt : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | Sltu : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | Xor : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | Srl : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | Sra : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | Or : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt
 | And : (rd : Register) -> (rs1 : Register) -> (rs2 : Register) -> Stmt

 | Lui : (rd : Register) -> (imm20 : Imm) -> Stmt

 | Beq : (rs1 : Register) -> (rs2 : Register) -> (sbimm12 : Label) -> Stmt
 | Bne : (rs1 : Register) -> (rs2 : Register) -> (sbimm12 : Label) -> Stmt
 | Blt : (rs1 : Register) -> (rs2 : Register) -> (sbimm12 : Label) -> Stmt
 | Bge : (rs1 : Register) -> (rs2 : Register) -> (sbimm12 : Label) -> Stmt
 | Bltu : (rs1 : Register) -> (rs2 : Register) -> (sbimm12 : Label) -> Stmt
 | Bgeu : (rs1 : Register) -> (rs2 : Register) -> (sbimm12 : Label) -> Stmt

 --| Jalr : (rd : Register) -> (rs1 : Register) -> (oimm12 : Imm) -> Stmt
 --| Jal : (rd : Register) -> (jimm20 : Imm) -> Stmt

 | seq : Stmt -> Stmt -> Stmt
 | skip : Stmt

infixr:90 "; " => Stmt.seq

/- # Word sizes (byte, half, word, dword)-/
inductive MachineWord : Type where
 | b
 | h
 | w


/- # RISC-V machine state types -/

/- ## Register values -/
def RVStateR : Type := Register -> MachineInt
/- ## Main memory address space -/
def RVStateM : Type := MachineAddr -> UInt8
/- ## Labeled instructions  -/
def RVStateI : Type := Label -> Stmt


open Register
open Stmt


#check Add a0 a1 a2; Sub a2 t0 t1; Xor sp tp fp; Lb a0 a1 (-2)


/-
# Small-step semantics

Our semantics will have judgments of the form:

`(S, sᵣ, sₘ, sᵢ) ⇒ (T, tᵣ, tₘ, tᵢ)`, meaning:

Starting in a state given by the contents of the registers (`sᵣ`), the main memory (`sₘ`) and a labeling of the program's instructions (`sᵢ`), executing one step of `S` leaves us in the state `(tᵣ, tₘ, tᵢ)`, with the program T remaining to be executed.

-/


/-
  ## Update registry state
 * Example: `sᵣ[a0 ↦ 0x1337][a1 ↦0xDEADBEEF]`
 -/
@[simp] def RVStateR.update (reg : Register) (val : MachineInt) (sᵣ : RVStateR) : RVStateR :=
  fun reg' => if reg' = reg then val else sᵣ reg'
macro s:term "[" reg:term "↦" val:term "]" : term =>
  `(RVStateR.update $reg $val $s)


/-
  ## Update memory bytes / halfwords / words
 * Example: `sₘ[0x1000 ↦ᵇ 64][0x0 ↦ʰ 2048][0x2000 ↦ʷ 0xCAFEBABE]`
 -/
@[simp] def RVStateM.updateB (addr : MachineAddr) (val : MachineInt) (sₘ : RVStateM) : RVStateM :=
  fun addr' => if addr' = addr
               then val |>.toUInt8 -- truncate value
               else sₘ addr'
macro s:term "[" addr:term "↦ᵇ" val:term "]" : term =>
  `(RVStateM.updateB $addr $val $s)

@[simp] def RVStateM.updateH (addr : MachineAddr) (val : MachineInt) (sₘ : RVStateM) : RVStateM :=
  fun addr' => let b2 := (val >>> 8).toUInt8
               let b1 := val.toUInt8
               if addr' = addr then b1
               else if addr' = addr + 1 then b2
               else sₘ addr'
macro s:term "[" addr:term "↦ʰ" val:term "]" : term =>
  `(RVStateM.updateH $addr $val $s)

@[simp] def RVStateM.updateW (addr : MachineAddr) (val : MachineInt) (sₘ : RVStateM) : RVStateM :=
  fun addr' => let b4 := (val >>> 24).toUInt8
               let b3 := (val >>> 16).toUInt8
               let b2 := (val >>> 8).toUInt8
               let b1 := val.toUInt8
               if addr' = addr then b1
               else if addr' = addr + 1 then b2
               else if addr' = addr + 2 then b3
               else if addr' = addr + 3 then b4
               else sₘ addr'
macro s:term "[" addr:term "↦ʷ" val:term "]" : term =>
  `(RVStateM.updateW $addr $val $s)


/-
  ## Retrieve memory bytes / halfwords / words
 * Example: `sᵣ[a0 ↦ sₘ ↾ᵇ 0x0][t0 ↦ sₘ ↑ʷ 0x1000][a1 ↦ sₘ ↑ʰ 0x5000]`
 * Interpretation: In the given registry state sᵣ and memory sₘ:
  ** register a0 holds the 0-extended value of the byte located at address 0x0 in memory.
  ** register t0 holds the value of the 32-bit word located at address 0x1000 in memory.
  ** register a1 holds the sign-extended value of the half word located at address 0x5000 in memory.
 -/
@[simp] def RVStateM.getB (sₘ : RVStateM) (addr : MachineAddr) : UInt8 :=
  sₘ addr
@[simp] def RVStateM.getH (sₘ : RVStateM) (addr : MachineAddr) : UInt16 :=
  combineBytes2 (sₘ addr + 1) (sₘ addr)
@[simp] def RVStateM.getW (sₘ : RVStateM) (addr : MachineAddr) : UInt32 :=
  combineBytes4 (sₘ addr + 3) (sₘ addr + 2) (sₘ addr + 1) (sₘ addr)

@[simp] def RVStateM.get (word : MachineWord) (sgnExt : Bool) (sₘ : RVStateM) (addr : MachineAddr) : MachineInt :=
  match word with
  | MachineWord.b => let x := RVStateM.getB sₘ addr
                     if sgnExt then signExtend8 x else x.toUInt32
  | MachineWord.h => let x := RVStateM.getH sₘ addr
                     if sgnExt then signExtend16 x else x.toUInt32
  | MachineWord.w => RVStateM.getW sₘ addr

/- ### 0-extended memory values ↾ -/
infix:90 " ↾ᵇ " => RVStateM.get MachineWord.b False
infix:90 " ↾ʰ " => RVStateM.get MachineWord.h False
infix:90 " ↾ʷ " => RVStateM.get MachineWord.w False

/- ### Sign-extended memory values ↑ -/
infix:90 " ↑ᵇ " => RVStateM.get MachineWord.b True
infix:90 " ↑ʰ " => RVStateM.get MachineWord.h True
infix:90 " ↑ʷ " => RVStateM.get MachineWord.w True


/- ### Signed comparisons -/
def RV.lt (x : MachineInt) (y : MachineInt) : Prop :=
    let msb₁ := x &&& (1 <<< 31).toUInt32
    let msb₂ := y &&& (1 <<< 31).toUInt32
    let x' : Int := match msb₁ with
                    | 0 => x |>.toNat |> Int.ofNat
                    | _ => - (twosComplement x |>.toNat |> Int.ofNat)
    let y' : Int := match msb₂ with
                    | 0 => y |>.toNat |> Int.ofNat
                    | _ => - (twosComplement y |>.toNat |> Int.ofNat)
    x' < y'
def RV.geq (x : MachineInt) (y : MachineInt) : Prop := ¬ RV.lt x y
infix:90 " <ˢ " => RV.lt
infix:90 " ≥ˢ " => RV.geq

/- ### Unsigned comparisons -/
def RV.ltu (x : MachineInt) (y : MachineInt) : Prop := x < y
def RV.gequ (x : MachineInt) (y : MachineInt) : Prop := ¬ RV.ltu x y
infix:90 " <ᵘ " => RV.ltu
infix:90 " ≥ᵘ " => RV.gequ

/- ### Arithmetic right shift -/
def RV.sra (x : MachineInt) (shAmt : Imm) :=
    let msb := x &&& (1 <<< 31).toUInt32
    let shifted := x >>> shAmt
    if (msb ≠ 0) then
      let mask := (2 ^ shAmt.toNat - 1).toUInt32
      shifted ||| (mask <<< (32 - shAmt))
    else
      shifted
infixl:65 " >>>ᵃ " => RV.sra


/-
## Derivation rules

* Assumptions:
  1. Conditional branches *must* be followed by an instruction (including `skip`), since the `Seq-step` derivation rule cannot be applied whenever a branch is taken. Therefore, such special cases  need to be specifically handled by their `<branch>-true` derivation rule.

  `Branch = {beq, bne, blt, bltu, bge, bgeu}`
-/

def Branch (S : Stmt) := match S with
  | Stmt.Beq _ _ _ => True
  | Stmt.Bne _ _ _ => True
  | Stmt.Blt _ _ _ => True
  | Stmt.Bltu _ _ _ => True
  | Stmt.Bge _ _ _ => True
  | Stmt.Bgeu _ _ _ => True
  | _ => False

#check add_left_inj

₀
₃

#synth IsRightCancelAdd UInt32

inductive SmallStep : Stmt × RVStateR × RVStateM × RVStateI -> Stmt × RVStateR × RVStateM × RVStateI -> Prop where

/-
### Special rules

    `(S, sᵣ, sₘ, sᵢ) ⇒ (S', sᵣ', sₘ', sᵢ), ¬ Branch S`
    `——————————————————————————————————————————————————  Seq-Step`
    `(S; T, sᵣ, sₘ, sᵢ) ⇒ (S'; T, sᵣ', sₘ', sᵢ)`

    `———————————————————————————————————————— Seq-Skip`
    `(skip; S, sᵣ, sₘ, sᵢ) ⇒ (S, sᵣ, sₘ, sᵢ)`
-/

  | seq_step (S sᵣ sₘ sᵢ S' sᵣ' sₘ' T) (h1 : SmallStep (S, sᵣ, sₘ, sᵢ) (S', sᵣ', sₘ', sᵢ)) (h2 : ¬ Branch S) :
    SmallStep (S; T, sᵣ, sₘ, sᵢ) (S'; T, sᵣ', sₘ', sᵢ)

  | seq_skip (S sᵣ sₘ sᵢ) :
    SmallStep (Stmt.skip; S, sᵣ, sₘ, sᵢ) (S, sᵣ, sₘ, sᵢ)

/-
### Store instructions

    `————————————————————————————————————————————————————————————————————————— sb`
    `(sb rs1 rs2 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ, sₘ[(sᵣ(rs1) + imm) ↦ᵇ sᵣ(rs2)], sᵢ)`

    `————————————————————————————————————————————————————————————————————————— sh`
    `(sh rs1 rs2 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ, sₘ[(sᵣ(rs1) + imm) ↦ʰ sᵣ(rs2)], sᵢ)`

    `————————————————————————————————————————————————————————————————————————— sw`
    `(sw rs1 rs2 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ, sₘ[(sᵣ(rs1) + imm) ↦ʷ sᵣ(rs2)], sᵢ)`
-/
  | sb (sᵣ sₘ sᵢ rs1 rs2 imm) :
    SmallStep (Stmt.Sb rs1 rs2 imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ, sₘ[(sᵣ rs1 + imm) ↦ᵇ sᵣ rs2], sᵢ)
  | sh (sᵣ sₘ sᵢ rs1 rs2 imm) :
    SmallStep (Stmt.Sh rs1 rs2 imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ, sₘ[(sᵣ rs1 + imm) ↦ʰ sᵣ rs2], sᵢ)
  | sw (sᵣ sₘ sᵢ rs1 rs2 imm) :
    SmallStep (Stmt.Sw rs1 rs2 imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ, sₘ[(sᵣ rs1 + imm) ↦ʷ sᵣ rs2], sᵢ)

/-
### Load instructions

    `————————————————————————————————————————————————————————————————————————— lui (WARNING: changed semantics - explain!)`
    `(lui rd imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ imm], sₘ, sᵢ)`

    `————————————————————————————————————————————————————————————————————————— lb`
    `(lb rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sₘ ↑ᵇ (sᵣ(rs1) + imm)], sₘ, sᵢ)`

    `————————————————————————————————————————————————————————————————————————— lh`
    `(lh rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sₘ ↑ʰ (sᵣ(rs1) + imm)], sₘ, sᵢ)`

    `————————————————————————————————————————————————————————————————————————— lw`
    `(lw rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sₘ ↑ʷ (sᵣ(rs1) + imm)], sₘ, sᵢ)`

    `————————————————————————————————————————————————————————————————————————— lbu`
    `(lbu rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sₘ ↾ᵇ (sᵣ(rs1) + imm)], sₘ, sᵢ)`

    `————————————————————————————————————————————————————————————————————————— lhu`
    `(lhu rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sₘ ↾ʰ (sᵣ(rs1) + imm)], sₘ, sᵢ)`
-/
  | lui (sᵣ sₘ sᵢ rd imm) :
    SmallStep (Stmt.Lui rd imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ[rd ↦ imm], sₘ, sᵢ)
  | lb (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Lb rd rs1 imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ[rd ↦ sₘ ↑ᵇ (sᵣ rs1 + imm)], sₘ, sᵢ)
  | lh (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Lh rd rs1 imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ[rd ↦ sₘ ↑ʰ (sᵣ rs1 + imm)], sₘ, sᵢ)
  | lw (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Lw rd rs1 imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ[rd ↦ sₘ ↑ʷ (sᵣ rs1 + imm)], sₘ, sᵢ)
  | lbu (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Lbu rd rs1 imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ[rd ↦ sₘ ↾ᵇ (sᵣ rs1 + imm)], sₘ, sᵢ)
  | lhu (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Lhu rd rs1 imm, sᵣ, sₘ, sᵢ) (Stmt.skip, sᵣ[rd ↦ sₘ ↾ʰ (sᵣ rs1 + imm)], sₘ, sᵢ)

/-
### Conditional branches

                      `sᵣ(rs1) = sᵣ(rs2)`
    `———————————————————————————————————————————————————— beq-true`
    `(beq rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (sᵢ(α), sᵣ, sₘ, sᵢ)`

                      `sᵣ(rs1) ≠ sᵣ(rs2)`
    `———————————————————————————————————————————————————— beq-false`
    `(beq rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (skip; T, sᵣ, sₘ, sᵢ)`


                      `sᵣ(rs1) ≠ sᵣ(rs2)`
    `———————————————————————————————————————————————————— bne-true`
    `(bne rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (sᵢ(α), sᵣ, sₘ, sᵢ)`

                      `sᵣ(rs1) = sᵣ(rs2)`
    `———————————————————————————————————————————————————— bne-false`
    `(beq rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (skip; T, sᵣ, sₘ, sᵢ)`
-/

  | beq_true (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : sᵣ rs1 = sᵣ rs2) :
    SmallStep (Stmt.Beq rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (sᵢ α, sᵣ, sₘ, sᵢ)
  | beq_false (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : sᵣ rs1 ≠ sᵣ rs2) :
    SmallStep (Stmt.Beq rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (Stmt.skip; T, sᵣ, sₘ, sᵢ)

  | bne_true (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : sᵣ rs1 ≠ sᵣ rs2) :
    SmallStep (Stmt.Bne rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (sᵢ α, sᵣ, sₘ, sᵢ)
  | bne_false (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : sᵣ rs1 = sᵣ rs2) :
    SmallStep (Stmt.Bne rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (Stmt.skip; T, sᵣ, sₘ, sᵢ)

/-
                        `sᵣ(rs1) <ˢ sᵣ(rs2)`
    `———————————————————————————————————————————————————— blt-true`
    `(blt rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (sᵢ(α), sᵣ, sₘ, sᵢ)`

                      `¬ (sᵣ(rs1) <ˢ sᵣ(rs2))`
    `———————————————————————————————————————————————————— blt-false`
    `(blt rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (skip; T, sᵣ, sₘ, sᵢ)`


                         `sᵣ(rs1) <ᵘ sᵣ(rs2)`
    `———————————————————————————————————————————————————— bltu-true`
    `(bltu rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (sᵢ(α), sᵣ, sₘ, sᵢ)`

                      `¬ (sᵣ(rs1) <ᵘ sᵣ(rs2))`
    `———————————————————————————————————————————————————— bltu-false`
    `(bltu rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (skip; T, sᵣ, sₘ, sᵢ)`
-/

  | blt_true (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : sᵣ rs1 <ˢ sᵣ rs2) :
    SmallStep (Stmt.Blt rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (sᵢ α, sᵣ, sₘ, sᵢ)
  | blt_false (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : ¬ (sᵣ rs1 <ˢ sᵣ rs2)) :
    SmallStep (Stmt.Blt rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (Stmt.skip; T, sᵣ, sₘ, sᵢ)

  | bltu_true (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : sᵣ rs1 <ᵘ sᵣ rs2) :
    SmallStep (Stmt.Bltu rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (sᵢ α, sᵣ, sₘ, sᵢ)
  | bltu_false (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : ¬ (sᵣ rs1 <ᵘ sᵣ rs2)) :
    SmallStep (Stmt.Bltu rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (Stmt.skip; T, sᵣ, sₘ, sᵢ)

/-
                         `sᵣ(rs1) ≥ˢ sᵣ(rs2)`
    `———————————————————————————————————————————————————— bge-true`
    `(bge rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (sᵢ(α), sᵣ, sₘ, sᵢ)`

                      `¬ (sᵣ(rs1) ≥ˢ sᵣ(rs2))`
    `———————————————————————————————————————————————————— bge-false`
    `(bge rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (skip; T, sᵣ, sₘ, sᵢ)`


                         `sᵣ(rs1) ≥ᵘ sᵣ(rs2)`
    `———————————————————————————————————————————————————— bgeu-true`
    `(bgeu rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (sᵢ(α), sᵣ, sₘ, sᵢ)`

                      `¬ (sᵣ(rs1) ≥ᵘ sᵣ(rs2))`
    `———————————————————————————————————————————————————— bgeu-false`
    `(bgeu rs1 rs2 α; T, sᵣ, sₘ, sᵢ) ⇒ (skip; T, sᵣ, sₘ, sᵢ)`
-/

  | bge_true (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : sᵣ rs1 ≥ˢ sᵣ rs2) :
    SmallStep (Stmt.Bge rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (sᵢ α, sᵣ, sₘ, sᵢ)
  | bge_false (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : ¬ (sᵣ rs1 ≥ˢ sᵣ rs2)) :
    SmallStep (Stmt.Bge rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (Stmt.skip; T, sᵣ, sₘ, sᵢ)

  | bgeu_true (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : sᵣ rs1 ≥ᵘ sᵣ rs2) :
    SmallStep (Stmt.Bgeu rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (sᵢ α, sᵣ, sₘ, sᵢ)
  | bgeu_false (sᵣ sₘ sᵢ rs1 rs2 α T) (hcond : ¬ (sᵣ rs1 ≥ᵘ sᵣ rs2)) :
    SmallStep (Stmt.Bgeu rs1 rs2 α; T, sᵣ, sₘ, sᵢ) (Stmt.skip; T, sᵣ, sₘ, sᵢ)

/-
### Integer computational instructions

`—————————————————————————————————————————————————————————————————————— addi`
`(addi rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) + imm], sₘ, sᵢ)`

                          `sᵣ(rs1) <ˢ imm`
`————————————————————————————————————————————————————————————— slti-true`
`(slti rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ 1], sₘ, sᵢ)`

                          `¬ (sᵣ(rs1) <ˢ imm)`
`————————————————————————————————————————————————————————————— slti-false`
`(slti rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ 0], sₘ, sᵢ)`

                          `sᵣ(rs1) <ᵘ imm`
`————————————————————————————————————————————————————————————— sltiu-true`
`(sltiu rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ 1], sₘ, sᵢ)`

                          `¬ (sᵣ(rs1) <ᵘ imm)`
`————————————————————————————————————————————————————————————— sltiu-false`
`(sltiu rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ 0], sₘ, sᵢ)`
-/
  | addi (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Addi rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 + imm], sₘ, sᵢ)

  | slti_true (sᵣ sₘ sᵢ rd rs1 imm) (hcond : sᵣ rs1 <ˢ imm) :
    SmallStep (Stmt.Slti rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ 1], sₘ, sᵢ)

  | slti_false (sᵣ sₘ sᵢ rd rs1 imm) (hcond : ¬(sᵣ rs1 <ˢ imm)) :
    SmallStep (Stmt.Slti rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ 0], sₘ, sᵢ)

  | sltiu_true (sᵣ sₘ sᵢ rd rs1 imm) (hcond : sᵣ rs1 <ᵘ imm) :
    SmallStep (Stmt.Sltiu rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ 1], sₘ, sᵢ)

  | sltiu_false (sᵣ sₘ sᵢ rd rs1 imm) (hcond : ¬(sᵣ rs1 <ᵘ imm)) :
    SmallStep (Stmt.Sltiu rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ 0], sₘ, sᵢ)


/-
`—————————————————————————————————————————————————————————————————————— andi`
`(andi rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) &&& imm], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— ori`
`(ori rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) ||| imm], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— xori`
`(xori rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) ^^^ imm], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— slli`
`(slli rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) <<< imm], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— srli`
`(srli rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) >>> imm], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— srai`
`(srai rd rs1 imm, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) >>>ᵃ imm], sₘ, sᵢ)`
-/

  | andi (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Andi rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 &&& imm], sₘ, sᵢ)

  | ori (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Ori rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 ||| imm], sₘ, sᵢ)

  | xori (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Xori rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 ^^^ imm], sₘ, sᵢ)

  | slli (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Slli rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 <<< imm], sₘ, sᵢ)

  | srli (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Srli rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 >>> imm], sₘ, sᵢ)

  | srai (sᵣ sₘ sᵢ rd rs1 imm) :
    SmallStep (Stmt.Srai rd rs1 imm, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 >>>ᵃ imm], sₘ, sᵢ)

/-
`—————————————————————————————————————————————————————————————————————— add`
`(add rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) + sᵣ(rs2)], sₘ, sᵢ)`


                          `sᵣ(rs1) <ˢ sᵣ(rs2)`
`————————————————————————————————————————————————————————————— slt-true`
`(slt rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ 1], sₘ, sᵢ)`

                          `¬ (sᵣ(rs1) <ˢ sᵣ(rs2))`
`————————————————————————————————————————————————————————————— slt-false`
`(slt rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ 0], sₘ, sᵢ)`

                          `sᵣ(rs1) <ᵘ sᵣ(rs2)`
`————————————————————————————————————————————————————————————— sltu-true`
`(sltu rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ 1], sₘ, sᵢ)`

                          `¬ (sᵣ(rs1) <ᵘ sᵣ(rs2))`
`————————————————————————————————————————————————————————————— sltu-false`
`(sltu rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ 0], sₘ, sᵢ)`
-/

  | add (sᵣ sₘ sᵢ rd rs1 rs2) :
    SmallStep (Stmt.Add rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 + sᵣ rs2], sₘ, sᵢ)

  | slt_true (sᵣ sₘ sᵢ rd rs1 rs2) (hcond : sᵣ rs1 <ˢ sᵣ rs2) :
    SmallStep (Stmt.Slt rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ 1], sₘ, sᵢ)

  | slt_false (sᵣ sₘ sᵢ rd rs1 rs2) (hcond : ¬(sᵣ rs1 <ˢ sᵣ rs2)) :
    SmallStep (Stmt.Slt rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ 0], sₘ, sᵢ)

  | sltu_true (sᵣ sₘ sᵢ rd rs1 rs2) (hcond : sᵣ rs1 <ᵘ sᵣ rs2) :
    SmallStep (Stmt.Sltu rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ 1], sₘ, sᵢ)

  | sltu_false (sᵣ sₘ sᵢ rd rs1 rs2) (hcond : ¬(sᵣ rs1 <ᵘ sᵣ rs2)) :
    SmallStep (Stmt.Sltu rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ 0], sₘ, sᵢ)

/-
`—————————————————————————————————————————————————————————————————————— and`
`(and rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) &&& sᵣ(rs2)], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— or`
`(or rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) ||| sᵣ(rs2)], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— xor`
`(xor rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) ^^^ sᵣ(rs2)], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— sll`
`(sll rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) <<< sᵣ(rs2)], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— srl`
`(srl rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) >>> sᵣ(rs2)], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— sub`
`(sub rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) - sᵣ(rs2)], sₘ, sᵢ)`

`—————————————————————————————————————————————————————————————————————— sra`
`(sra rd rs1 rs2, sᵣ, sₘ, sᵢ) ⇒ (skip, sᵣ[rd ↦ sᵣ(rs1) >>>ᵃ sᵣ(rs2)], sₘ, sᵢ)`
-/

  | and (sᵣ sₘ sᵢ rd rs1 rs2) :
    SmallStep (Stmt.And rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 &&& sᵣ rs2], sₘ, sᵢ)

  | or (sᵣ sₘ sᵢ rd rs1 rs2) :
    SmallStep (Stmt.Or rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 ||| sᵣ rs2], sₘ, sᵢ)

  | xor (sᵣ sₘ sᵢ rd rs1 rs2) :
    SmallStep (Stmt.Xor rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 ^^^ sᵣ rs2], sₘ, sᵢ)

  | sll (sᵣ sₘ sᵢ rd rs1 rs2) :
    SmallStep (Stmt.Sll rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 <<< sᵣ rs2], sₘ, sᵢ)

  | srl (sᵣ sₘ sᵢ rd rs1 rs2) :
    SmallStep (Stmt.Srl rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 >>> sᵣ rs2], sₘ, sᵢ)

  | sub (sᵣ sₘ sᵢ rd rs1 rs2) :
    SmallStep (Stmt.Srl rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 - sᵣ rs2], sₘ, sᵢ)

  | sra (sᵣ sₘ sᵢ rd rs1 rs2) :
    SmallStep (Stmt.Sra rd rs1 rs2, sᵣ, sₘ, sᵢ) (skip, sᵣ[rd ↦ sᵣ rs1 >>>ᵃ sᵣ rs2], sₘ, sᵢ)


infixr:100 " ⇒ " => SmallStep
infixr:100 " ⇒* " => RTC SmallStep

-- # Theorems

/-
example (α β n m : ℕ) (hneq1 : α ≠ β + m - n := by decide) (hneq2 : n ≠ β + m - α := by decide) (hneq3 : β ≠ α + n - m := by decide) (hneq4 : m ≠ α + n - β := by decide) : α + n ≠ β + m := by
 aesop?

example (n : ℕ) : n ≠ n + 1 := by simp only [ne_eq, self_eq_add_right, one_ne_zero,
  not_false_eq_true]

example (n : UInt32) : n ≠ n + 1 := by simp?
-/

-- ## Registry state theorems
@[simp] theorem RVStateR.update_apply (reg : Register) (val : MachineInt) (sᵣ : RVStateR) :
  (sᵣ[reg ↦ val]) reg = val :=
  by
    apply if_pos
    rfl

@[simp] theorem RVStateR.update_apply_neq (reg reg' : Register) (val : MachineInt) (sᵣ : RVStateR)
    (hneq : reg' ≠ reg) :
  (sᵣ[reg ↦ val]) reg' = sᵣ reg' :=
  by
    apply if_neg
    assumption

@[simp] theorem RVStateR.update_override (reg : Register) (val₁ val₂ : MachineInt) (sᵣ : RVStateR) :
  sᵣ[reg ↦ val₂][reg ↦ val₁] = sᵣ[reg ↦ val₁] :=
  by
    apply funext
    intro reg'
    cases Classical.em (reg' = reg) with
    | inl h => simp [h]
    | inr h => simp only [ne_eq, h, not_false_eq_true, update_apply_neq]

--example (h : False := by trivial) : False := by
--  simp at h

theorem RVStateR.update_swap --(reg₁ reg₂ : Register) (val₁ val₂ : MachineInt) (sᵣ : RVStateR)
    (hneq : reg₁ ≠ reg₂ := by decide) :
  sᵣ[reg₂ ↦ val₂][reg₁ ↦ val₁] = sᵣ[reg₁ ↦ val₁][reg₂ ↦ val₂] :=
  by
    apply funext
    intro reg'
    cases Classical.em (reg' = reg₁) with
    | inl h => simp? [*]
    | inr h =>
      cases Classical.em (reg' = reg₂) with
      | inl h2 => aesop?
      | inr h2 => aesop?


@[simp] theorem RVStateR.update_id (reg : Register) (sᵣ : RVStateR) :
  sᵣ[reg ↦ sᵣ reg] = sᵣ :=
  by
    apply funext
    intro reg'
    simp [RVStateR.update]
    intro heq
    simp [*]

@[simp] theorem RVStateR.update_same_const (reg : Register) (val : MachineInt) :
  (λ _ ↦ val)[reg ↦ val] = (λ _ ↦ val) :=
  by
    apply funext
    simp [RVStateR.update]


-- ## Memory state theorems
@[simp] theorem RVStateM.update_applyB (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  (sₘ[α ↦ᵇ val]) α = val.toUInt8 :=
  by
    apply if_pos
    rfl
@[simp] theorem RVStateM.update_applyH_0 (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  (sₘ[α ↦ʰ val]) α = val.toUInt8 :=
  by
    apply if_pos
    rfl



theorem if_neg_pos
  {c1 c2 : Prop} {h : Decidable c1} {h' : Decidable c2}
  (hnc1 : ¬c1) (hnc2 : c2) {α : Sort u} {e1 e2 e3 : α} :
  (ite c1 e1 (ite c2 e2 e3)) = e2
  := by aesop

@[simp] theorem RVStateM.update_applyH_1 (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  (sₘ[α ↦ʰ val]) (α+1) = (val >>> 8).toUInt8 := by
  apply if_neg_pos
  { simp }
  { rfl }

@[simp] theorem RVStateM.update_H_eq_2B (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  sₘ[α ↦ʰ val] = sₘ[α ↦ᵇ val][α+1 ↦ᵇ val >>> 8] := by
    apply funext
    intro A'
    cases Classical.em (A' = α) with
    | inl h => simp? [*]
    | inr h =>
      cases Classical.em (A' = α+1) with
      | inl h2 => aesop?
      | inr h2 => aesop?



@[simp] theorem RVStateM.update_applyW_0 (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  (sₘ[α ↦ʷ val]) α = val.toUInt8 :=
  by
    apply if_pos
    rfl
@[simp] theorem RVStateM.update_applyW_1 (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  (sₘ[α ↦ʷ val]) (α+1) = (val >>> 8).toUInt8 := sorry
@[simp] theorem RVStateM.update_applyW_2 (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  (sₘ[α ↦ʷ val]) (α+2) = (val >>> 16).toUInt8 := sorry
@[simp] theorem RVStateM.update_applyW_3 (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  (sₘ[α ↦ʷ val]) (α+3) = (val >>> 24).toUInt8 := sorry


@[simp] theorem RVStateM.update_W_eq_4B (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  sₘ[α ↦ʷ val] = sₘ[α ↦ᵇ val][α+1 ↦ᵇ val >>> 8][α+2 ↦ᵇ val >>> 16][α+3 ↦ᵇ val >>> 24] := by
    apply funext
    intro α'
    cases Classical.em (α' = α) with
    | inl h => simp [h]
    | inr h =>
        cases Classical.em (α' = α + 1) with
        | inl h2 => simp? [h2]
        | inr h2 =>
            cases Classical.em (α' = α + 2) with
            | inl h3 => simp? [h3]
            | inr h3 =>
                  cases Classical.em (α' = α + 3) with
                  | inl h4 => simp? [h4]
                  | inr h4 => simp? [h, h2, h3]
@[simp] theorem RVStateM.update_W_eq_2H (α : MachineAddr) (val : MachineInt) (sₘ : RVStateM) :
  sₘ[α ↦ʷ val] = sₘ[α ↦ʰ val][α+2 ↦ʰ val >>> 16] := sorry



@[simp] theorem RVStateM.update_apply_neqB (α β : MachineAddr) (val : MachineInt) (sₘ : RVStateM)
    (hneq : β ≠ α) :
  (sₘ[α ↦ᵇ val]) β = sₘ β :=
  by
    apply if_neg
    assumption

/-
  * The above theorem does not hold for halfwords and words, since adjacent bytes are overwritten when applying memory state updates with word sizes larger than a byte.
  * Example: `(sₘ[α+1 ↦ᵇ 0xAA][α ↦ʰ 0xCAFE]) α+1 ≠ (sₘ[α+1 ↦ᵇ 0xAA]) α+1`
  * Solution: enforce the fact that α's and β's address ranges do not intersect.
-/

theorem double_if_neg
  {c1 c2 : Prop} {h : Decidable c1} {h' : Decidable c2}
  (hnc1 : ¬c1) (hnc2 : ¬c2) {α : Sort u} {e1 e2 e3 : α} :
  (ite c1 e1 (ite c2 e2 e3)) = e3
  := by aesop

@[simp] theorem RVStateM.update_apply_neqH (α β : MachineAddr) (val : MachineInt) (sₘ : RVStateM)
    (hneq1 : β ≠ α) (hneq2 : β ≠ α + 1) :
  (sₘ[α ↦ʰ val]) β = sₘ β :=
  by
    apply double_if_neg <;>
      assumption

theorem quadruple_if_neg {c1 c2 c3 c4 : Prop}
  {h : Decidable c1} {h' : Decidable c2} {h'' : Decidable c3} {h''' : Decidable c4}
  (hnc1 : ¬c1) (hnc2 : ¬c2) (hnc3 : ¬c3) (hnc4 : ¬c4)
  {α : Sort u} {e1 e2 e3 e4 e5 : α} :
  (ite c1 e1 (ite c2 e2 (ite c3 e3 (ite c4 e4 e5)))) = e5
  := by aesop

@[simp] theorem RVStateM.update_apply_neqW
  (α β : MachineAddr) (val : MachineInt) (sₘ : RVStateM)
  (hneq1 : β ≠ α) (hneq2 : β ≠ α + 1) (hneq3 : β ≠ α + 2) (hneq4 : β ≠ α + 3) :
  (sₘ[α ↦ʷ val]) β = sₘ β :=
  by
    apply quadruple_if_neg
    { assumption }
    { assumption }
    { assumption }
    { assumption }


@[simp] theorem RVStateM.update_overrideB (α : MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM) :
  sₘ[α ↦ᵇ val₂][α ↦ᵇ val₁] = sₘ[α ↦ᵇ val₁] :=
  by
    apply funext
    intro α'
    cases Classical.em (α' = α) with
    | inl h => simp [h]
    | inr h => simp [h]

@[simp] theorem RVStateM.update_overrideH (α : MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM) :
  sₘ[α ↦ʰ val₂][α ↦ʰ val₁] = sₘ[α ↦ʰ val₁] :=
  by
    apply funext
    intro α'
    cases Classical.em (α' = α) with
    | inl h => simp [h]
    | inr h =>
        cases Classical.em (α' = α + 1) with
        | inl h2 => simp? [h2]
        | inr h2 => simp? [h, h2]

@[simp] theorem RVStateM.update_overrideW (α : MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM) :
  sₘ[α ↦ʷ val₂][α ↦ʷ val₁] = sₘ[α ↦ʷ val₁] :=
  by
    apply funext
    intro α'
    cases Classical.em (α' = α) with
    | inl h => simp [h]
    | inr h =>
        cases Classical.em (α' = α + 1) with
        | inl h2 => simp? [h2]
        | inr h2 =>
            cases Classical.em (α' = α + 2) with
            | inl h3 => simp? [h3]
            | inr h3 =>
                  cases Classical.em (α' = α + 3) with
                  | inl h4 => simp? [h4]
                  | inr h4 => simp? [*]


--example (h : False := by trivial) : False := by
--  simp at h

@[simp] theorem RVStateM.update_swapB (α β: MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM)
    (hneq : α ≠ β := by decide) :
  sₘ[α ↦ᵇ val₂][β ↦ᵇ val₁] = sₘ[β ↦ᵇ val₁][α ↦ᵇ val₂] :=
  by
    apply funext
    intro γ
    cases Classical.em (γ = α) with
    | inl h => simp [*]
    | inr h =>
      cases Classical.em (γ = β) with
      | inl h2 => aesop
      | inr h2 => aesop

example (α β: MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM) (hneq : α ≠ β := by decide) :
  (sₘ[α ↦ᵇ val₁][β ↦ᵇ val₂]) α = val₁.toUInt8 := by simp? [*]

example (α : MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM) :
  (sₘ[α ↦ᵇ val₁][α+1 ↦ᵇ val₂]) α = val₁.toUInt8 := by simp? [*] -- should be valid

example (α β: MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM) (hneq : α ≠ β := by decide) :
  (sₘ[α ↦ᵇ val₁][β ↦ᵇ val₂][β+1 ↦ᵇ val₂])  α = val₁.toUInt8 := by simp? [*] -- NOT VALID!


example (α β: UInt32)
    (hneq : α ≠ β := by decide) (hneq1 : α ≠ β + 1 := by decide) (hneq2 : α ≠ β + 2 := by decide) (hneq3 : α ≠ β + 3 := by decide)
    (hneq4 : α + 1 ≠ β := by decide) (hneq5 : α + 2 ≠ β := by decide) (hneq6 : α + 3 ≠ β := by decide) :
    α + 1 ≠ β + 2 := by simp? [*]


example (α β: ℕ)
    (hneq : α ≠ β := by decide) (hneq1 : α ≠ β + 1 := by decide) (hneq2 : α ≠ β + 2 := by decide) (hneq3 : α ≠ β + 3 := by decide)
    (hneq4 : α + 1 ≠ β := by decide) (hneq5 : α + 2 ≠ β := by decide) (hneq6 : α + 3 ≠ β := by decide) :
    α + 1 ≠ β + 2 := by simp only [ne_eq, add_left_inj, not_false_eq_true, hneq1]

@[simp] theorem RVStateM.update_swapH (α β: MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM)
    (hneq : α ≠ β := by decide) (hneq1 : α ≠ β + 1 := by decide)
    (hneq2 : α + 1 ≠ β := by decide)  :
  sₘ[α ↦ʰ val₂][β ↦ʰ val₁] = sₘ[β ↦ʰ val₁][α ↦ʰ val₂] := by simp? [*]

@[simp] theorem RVStateM.update_swapW (α β: MachineAddr) (val₁ val₂ : MachineInt) (sₘ : RVStateM)
    (hneq : α ≠ β := by decide) (hneq1 : α ≠ β + 1 := by decide) (hneq2 : α ≠ β + 2 := by decide) (hneq3 : α ≠ β + 3 := by decide)
    (hneq4 : α + 1 ≠ β := by decide) (hneq5 : α + 2 ≠ β := by decide) (hneq6 : α + 3 ≠ β := by decide) :
  sₘ[α ↦ʷ val₂][β ↦ʷ val₁] = sₘ[β ↦ʷ val₁][α ↦ʷ val₂] :=
  by
    apply funext
    intro γ
    simp? [*]
    sorry -- TODO
    /-
    cases Classical.em (γ = α) with
    | inl h =>
        simp? [*]
    | inr h =>
      cases Classical.em (γ = β) with
      | inl h2 => aesop
      | inr h2 => aesop
    -/


@[simp] theorem uint8_to_uint32_eq (a : UInt8) : (a.toUInt32).toUInt8 = a := sorry


@[simp] theorem RVStateM.update_idB (α : MachineAddr) (sₘ : RVStateM) :
  sₘ[α ↦ᵇ sₘ α] = sₘ :=
  by
    apply funext
    intro α'
    simp
    intro heq
    simp [*]

@[simp] theorem RVStateM.update_same_constB (α : MachineAddr) (val : MachineInt) :
  (λ _ => val.toUInt8)[α ↦ᵇ val] = (λ _ => val.toUInt8) :=
  by
    apply funext
    intro α'
    simp



@[simp] theorem RVStateM.getB_apply (α : MachineAddr) (sₘ : RVStateM) :
  sₘ ↾ᵇ α = sₘ α := by simp

@[simp] theorem RVStateM.getH_eq_combine2B (α : MachineAddr) (sₘ : RVStateM) :
  sₘ ↾ʰ α = combineBytes2 (sₘ ↾ᵇ α+1) (sₘ ↾ᵇ α)  := by simp

@[simp] theorem RVStateM.getW_eq_combine4B (α : MachineAddr) (sₘ : RVStateM) :
  sₘ ↾ʷ α = combineBytes4 (sₘ ↾ᵇ α+3) (sₘ ↾ᵇ α+2) (sₘ ↾ᵇ α+1) (sₘ ↾ᵇ α)  := by simp

@[simp] theorem RVStateM.getB_sgnext (α : MachineAddr) (sₘ : RVStateM) :
  sₘ ↑ᵇ α = signExtend8 (sₘ α) := by simp

@[simp] theorem RVStateM.getH_sgnext_eq_combine2B (α : MachineAddr) (sₘ : RVStateM) :
  sₘ ↑ʰ α = signExtend16 (combineBytes2 (sₘ ↾ᵇ α+1) (sₘ ↾ᵇ α))  := by simp

@[simp] theorem RVStateM.getW_sgnext_eq_getW (α : MachineAddr) (sₘ : RVStateM) :
  sₘ ↑ʷ α = sₘ ↾ʷ α  := by simp


example : (skip; Add a0 a1 a2, λ _ => 0, λ _ => 0, λ _ => skip) ⇒ (Add a0 a1 a2, λ _ => 0, λ _ => 0, λ _ => skip) := by
  apply SmallStep.seq_skip



@[simp] theorem uint32_to_uint8_eq (val : UInt32) (hcond : ¬ val > 2^8 := by decide) : val.toUInt8.toUInt32 = val := sorry

@[simp] theorem sgnext8_id (hcond : val < 2^7 := by decide) : signExtend8 (val.toUInt8) = val := sorry

example : sₘ[α ↦ᵇ val] ↑ᵇ ((sᵣ[rd ↦ α]) rd) = signExtend8 val := by simp?

example : (λ _ => 0)[0x1000 ↦ᵇ 0x64] ↑ᵇ (((λ _ => 0)[rd ↦ 0x1000]) rd + 0) = 0x64 := by
  simp
  rw [sgnext8_id]

example  (hneq : rs1 ≠ rd := by decide) :
          (Lb rd rs1 0, (λ _ => 0)[rs1 ↦ 0x1000], (λ _ => 0)[0x1000 ↦ᵇ 64], λ _ => skip)
          ⇒
          (skip, (λ _ => 0)[rd ↦ 64][rs1 ↦ 0x1000], (λ _ => 0)[0x1000 ↦ᵇ 64], λ _ => skip) := by

    --set_option trace.Meta.Tactic.simp true in
    --rw_search
    --simp only [ne_eq, not_false_eq_true, RVStateR.update_swap _ _ _ _ _ hneq]
    rw [RVStateR.update_swap hneq]
    --rw [←sgnext8_id]
    --rw [←RVStateM.getB_sgnext, ←RVStateM.update_applyB]
    apply SmallStep.lb

example : p → q → p :=  λ hp : p => λ hq : q => hp

example (hneq : r2 ≠ r1) : (λ _ => 0)[r1 ↦ 64][r2 ↦ 0x1000] = (λ _ => 0)[r2 ↦ 0x1000][r1 ↦ 64] := by
  set_option trace.Meta.Tactic.simp.rewrite true in
  simp only [ne_eq, not_false_eq_true, RVStateR.update_swap hneq]




theorem pgA ( α β : MachineInt ) ( A₁ A₂ A₃ : MachineAddr )
             ( h1 : A₁ ≠ A₂ )  ( h2 : A₁ ≠ A₃ ) ( h3 : A₂ ≠ A₃ ) :
              (S, (λ _ => 0), (λ _ => 0)[A₁ ↦ᵇ α][A₂ ↦ᵇ β], sᵢ) ⇒*
              (skip, sᵣ', sₘ[A₃ ↦ᵇ α + β], sᵢ) := by

    apply RTC.head
    {apply SmallStep.lui
     apply SmallStep.seq_step
     apply SmallStep.seq_skip}
    { apply RTC.head }
      { apply ...}

theorem pgB ( α β : MachineInt ) ( A₁ A₂ A₃ : MachineAddr )
             ( h1 : A₁ ≠ A₂ )  ( h2 : A₁ ≠ A₃ ) ( h3 : A₂ ≠ A₃ ) :
              (V, (λ _ => 0), (λ _ => 0)[A₁ ↦ᵇ α][A₂ ↦ᵇ β], vᵢ) ⇒*
              (skip, vᵣ', sₘ[A₃ ↦ᵇ α + β], vᵢ) := by

    apply RTC.head
    {apply SmallStep.lbu
     simp [*]
     apply SmallStep.seq_step
     apply SmallStep.seq_skip}
    { apply RTC.head }
      { apply ...}
#check SmallStep.lb
